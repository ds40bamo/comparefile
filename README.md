# Description

Web project build with FastAPI, Express.js and React.js to assess and investigate the quality of summarization tasks.
You can evaluate the quality of your own summarizations or generate summarizations from texts or urls based on many metrics and summarizers.
A running demo can be found here: <https://tldr.demo.webis.de>.
