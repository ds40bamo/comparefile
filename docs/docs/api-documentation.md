---
title: Api Documentation
sidebar_position: 5
---

The api documentation can be found under `/api/doc` (e.g. <https://tldr.demo.webis.de/api/doc>).  

The script `summary-workbench.py`, which can be found in the root of the repository, can be used to access the application from the commandline.
It can also be imported in python files to build applications based on Summary Workbench.
